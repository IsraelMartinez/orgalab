#include <stdio.h>
#include <stdlib.h>
#include <string.h>
/*
programa.exe -arg1 arg2.xx

te dara 2 en argc, el primero es el nombre y direccion del programa, el segundo es el primer argumento y el tercero es el segundo argumento.

En argv es un poco diferente.

argv[1] -i
argv[2]  I
argv[3] -n
argv[4]  N
*/

int main(int argc, char** argv){

	printf("argc = %i\n", argc);
	printf("argv[0] = %s\n", argv[0]);
	printf("argv[1] = %s\n", argv[1]);
	printf("argv[2] = %s\n", argv[2]);
	printf("argv[3] = %s\n", argv[3]);
	printf("argv[4] = %s\n", argv[4]);

	int I = 0;
	double N = 0;
	int valido = 1; 
	if(argc > 5){
			printf(" Extensión del comando no permitido máximo 4 terminos > \n");
			printf("Comando correcto es Correcto es ~ $ ./lnX -i I -n N  >\n");
			printf("1) Con I =  Número de iteraciones de la función ln (soporta números naturales)\n");
			printf("2) Con N =  Número a evaluar en la función ln (soporta double)\n");
	}else{//intentar convertir a entero el valor I y en double
		//para las iteraciones I
		if(strncmp(argv[1],"-i",32) == 0){

			if(atoi(argv[2])){
				I = atoi(argv[2]);
			}else{
				printf("Comando correcto es Correcto es ~ $ ./lnX -i I -n N  >\n");
				printf("I debe ser número natural > \n");
				valido = 0;
			}
		}
		//para el valor a evaluar N
		if(strncmp(argv[3],"-n",32) == 0){

			if(atoi(argv[4])){
				N = atoi(argv[4]);
			}else{
				printf("Comando correcto es Correcto es ~ $ ./lnX -i I -n N  >\n");
				printf("N debe ser número double del intervalo [1-3] para una precición de 5 decimales >\n");
				valido = 0;
			}
		}
		//Ahora se puede ejecutar la función ln si no hay errores
		if(valido == 1){
			
		}
	}
	printf("%i\n", strncmp("Hola","hola",32));
	return 0;
}