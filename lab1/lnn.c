#include <stdio.h>
//ĹIBRERÍAS DEL GETOPT()
#include <stdlib.h>
#include <unistd.j>

static double c0  = 1.0000000000000000000000000000000; 
static double c1  = 0.3333333333333333148296162562474; 
static double c2  = 0.2000000000000000111022302462516; 
static double c3  = 0.1428571428571428492126926812489; 
static double c4  = 0.1111111111111111049432054187491; 
static double c5  = 0.0909090909090909116141432377844; 
static double c6  = 0.0769230769230769273470116331737; 
static double c7  = 0.0666666666666666657414808128124; 
static double c8  = 0.0588235294117647050660124818933; 
static double c9  = 0.0526315789473684181309920404601; 
static double c10  = 0.0476190476190476164042308937496; 
static double c11  = 0.0434782608695652161845401906248; 
static double c12  = 0.0400000000000000008326672684689; 
static double c13  = 0.0370370370370370349810684729164; 
static double c14  = 0.0344827586206896546938693859374; 
static double c15  = 0.0322580645161290313627233672378; 
static double c16  = 0.0303030303030303038713810792615; 
static double c17  = 0.0285714285714285705364279266405; 
static double c18  = 0.0270270270270270285273284116556; 
static double c19  = 0.0256410256410256401360392430888; 
static double c20  = 0.0243902439024390252364504760862; 
static double c21  = 0.0232558139534883717702840044694; 
static double c22  = 0.0222222222222222230703092549220; 
static double c23  = 0.0212765957446808505471036454537; 
static double c24  = 0.0204081632653061208204636756136; 
static double c25  = 0.0196078431372549016886708272978; 
static double c26  = 0.0188679245283018860723789345002; 
static double c27  = 0.0181818181818181809350498667754; 
static double c28  = 0.0175438596491228060436640134867; 
static double c29  = 0.0169491525423728812971280177635; 
static double c30  = 0.0163934426229508205252738406443;


double ln(double x){
    x=(x-1)/(x+1);
    return 2*(c0 * x
         + c1 * x * x * x
         + c2 * x * x * x * x * x
         + c3 * x * x * x * x * x * x * x
         + c4 * x * x * x * x * x * x * x * x * x
         + c5 * x * x * x * x * x * x * x * x * x * x * x
         + c6 * x * x * x * x * x * x * x * x * x * x * x * x * x
         + c7 * x * x * x * x * x * x * x * x * x * x * x * x * x * x * x); 
}

int debug = 0;

int main(int argc, char **argv)
{
	extern char *optarg;
	extern int optind;
	int c, err = 0;


    int iteraciones = 100000000;
    double x = 2;
    
    double ultimo = 0;
    int i;
    for(i = 0; i < iteraciones; i++)
    {
        ultimo = ln(x);
    }

    printf("%f\n", ultimo);
}
