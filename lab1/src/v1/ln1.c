#include <stdio.h> /* para printf */
#include <stdlib.h> /* para exit */
#define _GNU_SOURCE
#include <getopt.h> /*para getopt_long*/ 

static double c0  = 1.0000000000000000000000000000000; 
static double c1  = 0.3333333333333333148296162562474; 
static double c2  = 0.2000000000000000111022302462516; 
static double c3  = 0.1428571428571428492126926812489; 
static double c4  = 0.1111111111111111049432054187491; 
static double c5  = 0.0909090909090909116141432377844; 
static double c6  = 0.0769230769230769273470116331737; 
static double c7  = 0.0666666666666666657414808128124;


double ln(double x){
    x=(x-1)/(x+1);
    return 2*(c0 * x
         + c1 * x * (x * x)
         + c2 * x * (x * x) * (x * x)
         + c3 * x * (x * x) * (x * x) * (x * x)
         + c4 * x * (x * x) * (x * x) * (x * x) * (x * x)
         + c5 * x * (x * x) * (x * x) * (x * x) * (x * x) * (x * x)
         + c6 * x * (x * x) * (x * x) * (x * x) * (x * x) * (x * x) * (x * x)
         + c7 * x * (x * x) * (x * x) * (x * x) * (x * x) * (x * x) * (x * x) * (x * x));
}


int main(int argc, char * argv[]){
int c;
int digit_optind = 0;
//Para los valores obtenidos por linea de comandos
int I = 0;
double N = 0;
int valido = 1;
int i;
double ultimo;
 while (1) {
     /*Este while parsea todos los argumentos de argv con la función getopt_long()*/
     int this_option_optind = optind ? optind : 1;
     int option_index = 0;
     /*Esta estructura sirve para convertir los argumentos largos en cortos
     así, si el argumento es --compress en c se guardará 'c', como c tiene
     un argumento opcional, si éste existe se guarda en optarg.
     no_argument, optional_argument y required_argument son macros que se
     expanden a 0, 1 y 2 respectivamente*/
     static struct option long_options[] = {
         {"iteracion", optional_argument, 0, 'i'},
         {"numero", required_argument, 0, 'n'},
         {"help", no_argument, 0, 'h'},
         {0, 0, 0, 0}
     };
    /*La cadena "dhvf:c::" indica que d, h y v no tienen argumentos, f tiene un
    argumento requerido (por eso los dos puntos 'f:') y c tiene un argumento opcional
    (de ahi los dobles dos puntos 'c::')*/
     c = getopt_long (argc, argv, "i:n:h",
     long_options, &option_index);
     if (c == -1)
     /*c es -1 cuando se terminan todos los argumentos, entonces sale del while*/
     break; 

     switch (c) { 
         case 'i':
             /*Si uno de los argumentos de main es --compress=LZW o -cLZW entra en este case
             c vale 'c' y optarg vale "LZW"*/
             /*printf ("option i\n");
             if (0 != optarg)
             printf(" con argumento %s", optarg);
             */
             if(atoi(optarg)){
                I = atoi(optarg);
                if(I < 1){
                    printf("ERROR >>>\n");
                    printf(" I debe ser un número natural >>>\n");
                    valido = 0;
                }
            }else{
                printf("ERROR >\n");
                printf("I debe ser número natural >>>\n");
                valido = 0;
            }
             break; 

         case 'n': 
             /*Si uno de los argumentos de main es --decompress o -d entra en este case*/
             /*printf ("option n\n");
             if (0 != optarg)
             printf(" con argumento %s", optarg);
             */
             if(atoi(optarg)){
                N = atof(optarg);
                if(N <= 0){
                    printf("ERROR >>>\n");
                    printf("N debe ser un número entero o decimal mayor que 0 >>>\n");
                    valido = 0;
                }
            }else{
                printf("ERROR >>>\n");
                printf("N debe ser número entero o decimal mayor que cero, además intervalo ]0-3] para una precición de 5 decimales >>>\n");
                valido = 0;
            }
             printf("\n");
             break;

         case 'h':
             /*y asi siguiendo..*/
             //printf ("option h\n");
             printf("Comando  de entrada >>> ~ $ ./lnX -i I -n N \n");
             printf("1) Con I =  Número de iteraciones de la función ln (soporta números naturales)\n");
             printf("2) Con N =  Número a evaluar en la función ln (soporta double) >>>\n");
             break;

         default:
             /*Si el argumento no machea con ninguna opción conocida, debería ser un error en los
             parámetros...*/
             printf ("La función getopt_long ha retornado un carácter desconocido. El carácter es = %c\n", c);
         }
     }
     /*Si siguen apareciendo cosas que no son argumentos, se imprimen hasta que se acaben...*/
     if (optind < argc) {
        printf ("No son opciones pero estan en ARGV: ");
        while (optind < argc)
            printf ("%s ", argv[optind++]);
        printf ("\n");
     }
     //INCLUIR LA FUNCIÓN  LN hacer bifurcación
     if(valido == 1){
        if(I != 0 && N != 0 ){

            for(i = 0; i < I; i++)
            {
                ultimo = ln(N);
            }

            printf("##########  %f ########## \n", ultimo);    
        }else{
            printf(" Uno de los parámetros falta >>>\n");
            printf("Para obtener ayuda ejecute ./lnX -h  ó --help (X = versión de la función)\n");
        }
        
    }else{
        printf("Para obtener ayuda ejecute ./lnX -h  ó --help (X = versión de la función)\n");
    }
//exit(0);
return 0;
}