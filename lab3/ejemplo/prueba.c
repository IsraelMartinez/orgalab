#include <xmmintrin.h>
#include <emmintrin.h>
#include <pmmintrin.h>
#include <stdio.h>

#define N 4
/*
	0 <<-
	1
	2
	3

	4 <<-
	5
	6
	7
	8
*/
int main(int argc, char const *argv[])
{
	float a[N] __attribute__((aligned(16)));
	//inicializar arreglo
	/*
	int i;
	for(i = 0;i < N; i++)}{
		a[i] = i + 1;
	}
	*/
	a[0] = 1;
	a[1] = 2;
	a[2] = 3;
	a[3] = 4;	
	/*
	
	for(i=0; i< N; i = i + 8){
		__m128 v = _mm_load_ps(&a[i]);
		__m128 v2 = _mm_load_ps(&a[i + 4]);

		v  = _mm_sqrt_ps(v);
		v2  = _mm_sqrt_ps(v2);
	}
	*/
	__m128 v = _mm_load_ps(a);
	v = _mm_sqrt_ps(v);
	_mm_store_ps(a, v);

	printf("%f\n",v[0]);
	printf("%f\n",v[1]);
	printf("%f\n",v[2]);
	printf("%f\n",v[3]);
	printf("##################\n");
	printf("%f\n",a[0]);
	printf("%f\n",a[1]);
	printf("%f\n",a[2]);
	printf("%f\n",a[3]);

	return 0;
}