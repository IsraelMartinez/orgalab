#include <stdio.h> /* para printf */
#include <stdlib.h> /* para exit */
#define _GNU_SOURCE
#include <getopt.h> /*para getopt_long*/ 
#include <math.h> 

#define MAX 100000000 //100.000.000

double funcion1EscalarDouble(double vector[], int tam,  int desde){
    double sum = 0, elem = 0;
    int i;
    for(i = desde; i < tam; i++){
        //elem = pow(sqrt(vector[i]),vector[i]);
        elem = pow(vector[i],(vector[i]/2));//solo empleamos la funcion potencia
        sum = sum + elem;//acumulamos el resultado
        printf("%f\n",sum);
    }
    return sum;
}

float funcion1Escalar(float vector[], int tam,  int desde){
    float sum = 0, elem = 0;
    int i;
    for(i = desde; i < tam; i++){
        //elem = pow(sqrt(vector[i]),vector[i]);
        elem = pow(vector[i],(vector[i]/2));//solo empleamos la funcion potencia
        sum = sum + elem;//acumulamos el resultado
        printf("%f\n",sum);
    }
    return sum;
}

double readFile(char* myFile){
    FILE *f = fopen(myFile, "r");

    if (f==NULL)
    {
       printf("%s\n",myFile );
       return -1;
    }

    char cadena[100]; /* Un array lo suficientemente grande como para guardar la línea más larga del fichero */
    //obtener cantidad de datos
    int i = 0;
    while (fgets(cadena, 100, f) != NULL)
       i++;
    fclose(f);
    int tam = i;
    //float vector [i];
    double vector [i];
    i = 0;
    fopen(myFile, "r");
    while (fgets(cadena, 100, f) != NULL)
    {
        vector[i] = atof(cadena);
        i++;
    }
    fclose(f);
    //Operamos sobre el arreglo
     return funcion1EscalarDouble(vector,tam,0);//obtenemos el resultado da cada elemento del vector
}

int main(int argc, char * argv[]){
    int c;
    int digit_optind = 0;
    //Para los valores obtenidos por linea de comandos
    char* myFile = NULL;
    int valido = 1;
    int i;
    double ultimo;
     while (1) {
         /*Este while parsea todos los argumentos de argv con la función getopt_long()*/
         int this_option_optind = optind ? optind : 1;
         int option_index = 0;
         /*Esta estructura sirve para convertir los argumentos largos en cortos
         así, si el argumento es --compress en c se guardará 'c', como c tiene
         un argumento opcional, si éste existe se guarda en optarg.
         no_argument, optional_argument y required_argument son macros que se
         expanden a 0, 1 y 2 respectivamente*/
         static struct option long_options[] = {
             {"file",required_argument, 0, 'f'},
             {"help", no_argument, 0, 'h'},
             {0, 0, 0, 0}
         };
        /*La cadena "dhvf:c::" indica que d, h y v no tienen argumentos, f tiene un
        argumento requerido (por eso los dos puntos 'f:') y c tiene un argumento opcional
        (de ahi los dobles dos puntos 'c::')*/
         c = getopt_long (argc, argv, "f:h",
         long_options, &option_index);
         if (c == -1)
         /*c es -1 cuando se terminan todos los argumentos, entonces sale del while*/
         break; 

         switch (c) { 
             case 'f':
                 myFile = optarg; //Guardamos la dirección ingresada
                 printf("Intentando leer archivo %s\n", myFile);
                 break;

             case 'h':
                 /*y asi siguiendo..*/
                 //printf ("option h\n");
                 printf("Comando  de entrada >>> ~ $ ./nombrePrograma -i I -n N \n");
                 printf("1) Con I =  Número de iteraciones de la función ln (soporta números naturales)\n");
                 printf("2) Con N =  Número a evaluar en la función ln (soporta double) >>>\n");
                 break;

             default:
                 /*Si el argumento no machea con ninguna opción conocida, debería ser un error en los
                 parámetros...*/
                 valido = 0;
                 printf ("La función getopt_long ha retornado un carácter desconocido. El carácter es = %c\n", c);
             }
         }
         /*Si siguen apareciendo cosas que no son argumentos, se imprimen hasta que se acaben...*/
         if (optind < argc) {
            printf ("No son opciones pero estan en ARGV: ");
            while (optind < argc)
                printf ("%s ", argv[optind++]);
            printf ("\n");
         }
         //INCLUIR LA FUNCIÓN  LN hacer bifurcación
         if(valido == 1){
            if(myFile != NULL){
    //########################## EJECUCION DEL PROGRAMA ###############################
                double resultado = readFile(myFile);
                printf("##########  %f ########## \n", resultado);
    //######################### FIN EJECUCION DEL PROGRAMA ############################    
            }else{
                printf(" Uno de los parámetros falta >>>\n");
                printf("Para obtener ayuda ejecute ./nombre -h  ó --help\n");
            }
        }else{
            printf("Para obtener ayuda ejecute ./nombre -h  ó --help\n");
        }
    //exit(0);
    return 0;
}