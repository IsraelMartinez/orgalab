#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

float getModulo(float vector[],float n){
    int i;
    float modulo=0;
    for(i=0;i<n;i++){
        if(vector[i]=='\0') break;
        modulo = (float)(modulo + pow(vector[i], 2));
    }

    modulo = sqrt(modulo);
    return modulo;
}

float getSumatoria(float vector[], float modulo, int n){
    float sumatoria = 0;
    int i;
    for(i=0;i<n-1;i++){
         if(i<modulo){
            sumatoria = sumatoria + vector[i]*vector[i+1];
        }
    }
    return sumatoria;
}

int main(int argc, char const *argv[])
{
	char* myFile = "data3.txt";
	FILE *f = malloc(sizeof(FILE *));
	f= fopen(myFile, "r");

	if (f==NULL)
	{
	   perror ("Error al abrir fichero.txt");
	   return -1;
	}

	char cadena[100]; /* Un array lo suficientemente grande como para guardar la línea más larga del fichero */
	int i = 1;
	while (fgets(cadena, 100, f) != NULL)
	   i++;
	fclose(f);

	float vector[i];
	
	i=0;
	fopen(myFile, "r");
	while (fgets(cadena, 100, f) != NULL)
	{
	   vector[i] = atof(cadena);
	   i++;
	}
	fclose(f);
    	int n = sizeof(vector)/sizeof(vector[0]);
    	float modulo = getModulo(vector,n);
    	float sumatoria = getSumatoria(vector,modulo,n);
    	printf("%f\n",sumatoria);

	return 0;
}
