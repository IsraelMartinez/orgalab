/*Ayuda pa los brocas*/
#include <xmmintrin.h>
#include <emmintrin.h>
#include <pmmintrin.h>
#include <stdio.h>

#define N 8

int main(int argc, char const *argv[])
{
	float a[N] __attribute__((aligned(16)));
/*
	a[0] = 1;
	a[1] = 2;
	a[2] = 3;
	a[3] = 4;
*/
	for(size_t i =0;i<N;i++)
	{
		a[i] = i+1;
	}

	for(size_t i =0;i<N;i=i+4)
	{
		__m128 v = _mm_load_ps(&a[i]);
		v = _mm_mul_ps(v,v);
		_mm_store_ps(&a[i],v);
	}
/*EN CASO DE QUE EL VALOR DE N NO SEA MULTIPLO DE 4 TIENEN QUE HACER EL RESTO DE FORMA ESCALAR.*/

	/**CALCULAR RAIZ CUADRADA DE 2 VECTORES Y SUMARLOS*/
	/*for(size_t i =0;i<N;i++)
	{
		a[i] = i+1;
	}

	__m128 acc;
	
	for(size_t i =0;i<N;i=i+8)
	{
		__m128 v = _mm_load_ps(&a[i]);
		__m128 v2 = _mm_load_ps(&a[i+4]);
		v = _mm_sqrt_ps(v);
		v2 = _mm_sqrt_ps(v2);

		acc = _mm_add_ps(acc,_mm_add_ps(v,v2));
	}
	_mm_store_ps(a,acc);
*/
/*
	__m128 v = _mm_load_ps(a);
	v = _mm_sqrt_ps(v);
	_mm_store_ps(a,v);
*/
	printf("%f %f %f %f %f %f %f\n",a[0],a[1],a[2],a[3],a[4],a[5],a[6]);

/*	printf("%f\n", a[0]+a[1]+a[2]+a[3] );
*/	return 0;
}